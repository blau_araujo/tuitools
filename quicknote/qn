#!/usr/bin/env bash
# ---------------------------------------------------------------------
# Script   : qnote
# Descrição: Simples gerenciador de notas rápidas
# Versão   : 0.0.1
# Data     : 25/08/2020
# Licença  : GNU/GPL v3.0
# ---------------------------------------------------------------------
# Copyright (C) 2020  Blau Araujo <blau@debxp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------
# Uso: qnote [opções] - sem opções, apenas lista as notas
# ---------------------------------------------------------------------

QN_FILE="$HOME/.qnotes"

>> $QN_FILE

menu_cmd='fzf --height=15 --border --reverse -e -i --tiebreak=begin --prompt=Nota:'

case ${1,,} in
       new|n) QN_MODE=0;;
      edit|e) QN_MODE=1;;
       del|d) QN_MODE=2;;
           *) QN_MODE=3;;
esac

draw_line() {
    printf -v line "%$(tput cols)s"
    printf "\e[38;5;240m${line// /─}\e[0m"
}

print_note() {
    draw_line
    printf "\e[35;1mData:\e[0m $1\n"
    printf "\e[35;1mNota:\e[0m $2\n"
    draw_line
}

get_notes() {
    local count=1
    local line
    while read line; do
        QN_LINES+="${count}. $line"$'\n'
        ((count++))
    done < $QN_FILE
}


[[ $QN_MODE -ne 0 ]] && {

    get_notes

    sel=$($menu_cmd <<< "$QN_LINES")

    [[ $sel ]] && {

        note_number=${sel%%. *}
        sel=${sel#*. }
        date=${sel%% *}
        text=${sel#* }

        [[ $QN_MODE -eq 3 ]] && print_note "$date" "$text"

        [[ $QN_MODE -eq 2 ]] && {
            printf "\e[33;1mQuer mesmo excluir a nota abaixo?\e[0m\n"
            sed "${note_number}q;d" $QN_FILE
            printf "\e[33;1mConfirmar (s/N)? \e[0m"
            read -N1
            [[ ${REPLY,,} = 's' ]] && {
                sed -i "${note_number}d" $QN_FILE && printf "\n\nNota removida...\n"
            } || {
                printf "\nNada removido!\n"
            }
        }

        [[ $QN_MODE -eq 1 ]] && {
            printf "\e[33;1mAltere o texto da nota e tecle 'Enter'...\e[0m\n"
            read -ei "$text" new_text
            [[ "$new_text" != "$text" ]] && {
                printf "\e[33;1mDeseja manter a data original (s/N)?\e[0m "
                read -N1
                case ${REPLY,,} in
                    s) new_date="$date"; echo;;
                    *) new_date=$(date '+%d/%m/%Y-%H:%M');;
                esac
                new_line="$new_date $new_text"
                sed -i "${note_number}s|.*|${new_line}|" $QN_FILE
                new_sel=$(sed -e "${note_number}q;d" $QN_FILE)
                print_note "${new_sel%% *}" "${new_sel#* }"
            } || {
                printf "\nNada alterado!\n"
            }
        }

    } || {

        exit 0

    }

} || {

    printf "Se a nota estiver vazia, nada será salvo.\nDigite sua nota: "
    read nota

    [[ $nota ]] && echo "$(date '+%d/%m/%Y-%H:%M') $nota" >> $QN_FILE && {
        sel=$(sed -e "$(wc -l < $QN_FILE)q;d" $QN_FILE)
        print_note "${sel%% *}" "${sel#* }"
    }
}

